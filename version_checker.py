import argparse
import re
import requests
import sys
import yaml
from pkg_resources import parse_version

exit = 0

parser = argparse.ArgumentParser()
parser.add_argument('--config', required=True)
args = parser.parse_args()

config_data = yaml.safe_load(open(args.config))


def get_max_version(list_of_versions):
    ver_list = [parse_version(ver) for ver in list_of_versions]
    return max(ver_list)


repo_url = config_data['repo']['url']
repo_request = requests.get(repo_url+'/index-v1.json')

apps_data = config_data['applications']
for app in apps_data:
    print(app)
    app_request = requests.get(apps_data[app]['url'])
    site = app_request.text
    upstream_versions = re.findall(apps_data[app]['regex'], site)
    app_name = apps_data[app]['packageName']
    app_data = repo_request.json()['packages'][app_name]
    repo_versions = [x['versionName'] for x in app_data]
    upstream_version = get_max_version(upstream_versions)
    repo_version = get_max_version(repo_versions)
    print(' ├─ Upstream version: {}'.format(upstream_version))
    print(' └─ Repo version: {}\n'.format(repo_version))
    if upstream_version > repo_version:
        exit = 1

sys.exit(exit)
