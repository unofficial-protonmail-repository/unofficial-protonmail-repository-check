[![repo status](https://gitlab.com/unofficial-protonmail-repository/unofficial-protonmail-repository-check/badges/master/pipeline.svg)](https://gitlab.com/unofficial-protonmail-repository/unofficial-protonmail-repository-check/commits/master)

## unoffical ProtonMail repository version check

This project monitors the status of [an unoffical ProtonMail repository][0] ([GitLab project][1]).  If the pipeline status is passing then the reposority has the most recent version from [ProtonApps][2].  Note, the packages are renamed to include version nubmers.  Older packages came from [APKmirror][3] and the packages are renamed to get under the 100 character limit in [Repomaker][4].


[0]: https://unofficial-protonmail-repository.gitlab.io/unofficial-protonmail-repository/fdroid/repo/index.html
[1]: https://gitlab.com/unofficial-protonmail-repository/unofficial-protonmail-repository
[2]: https://protonapps.com/protonmail-android
[3]: https://www.apkmirror.com/apk/protonmail/protonmail-encrypted-email
[4]: https://f-droid.org/en/repomaker/
